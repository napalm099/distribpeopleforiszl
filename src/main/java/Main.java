import com.fasterxml.jackson.databind.ObjectMapper;
import data.PropsReader;
import data.filesUtil.CsvPplList;
import data.struct.Plan;
import domain.Calculate;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;


public class Main {


    final static Logger logger = Logger.getLogger(Main.class);
    final static String PROP_FILE = "src/main/resources/application.properties";
    static String fileName = "plans_dd66+";

    public static void main(String[] args) {
        logger.info("try to read properties");
        PropsReader propsReader = PropsReader.getInstance();
        try {
            propsReader.load(new File(PROP_FILE));
        } catch (IOException e) {
            logger.error("properties file could' not read", e);
        }

        logger.info("properties read successful");

        logger.info("try to find plan file");
        ObjectMapper objectMapper = new ObjectMapper();
        Plan plansDD = null;
        try {
            plansDD = objectMapper.readValue(new File("src/main/resources/"+fileName+".json"), Plan.class);
        } catch (IOException e) {
            logger.error("plan file could' not read", e);
        }
        logger.info("plan file find");

        logger.info(String.format( "plan is valid: %b", plansDD.isValid()));


        logger.info("start calculating plan");

        Calculate calculate = new Calculate(plansDD, new CsvPplList(), propsReader.readParam("file.resultFilePath") + fileName +".csv");
        calculate.calc();

        logger.info("done!");


    }
}
