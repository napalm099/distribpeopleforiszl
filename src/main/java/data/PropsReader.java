package data;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Optional;
import java.util.Properties;

public class PropsReader {
    final static Logger logger = Logger.getLogger(PropsReader.class);
    Properties properties = new Properties();

    private static PropsReader propsReader = null;


    private PropsReader() {
    }

    public static PropsReader getInstance() {
        if (propsReader == null) {
            propsReader = new PropsReader();

        }
        return propsReader;
    }


    public void load(File propsFile) throws IOException {
        properties.load(new FileInputStream(propsFile));
    }

    public String readParam(String paramName) {

        String param = null;
        try {
            param = Optional.of(properties.getProperty(paramName)).orElseThrow(() -> new ParameterWrongFormat(paramName));
        } catch (Exception e) {
            logger.error("param could' not read ", e);
        }
        return param;
    }

    public static class ParameterWrongFormat extends Exception {
        ParameterWrongFormat(String paramName) {
            super(paramName);
        }

    }
}
