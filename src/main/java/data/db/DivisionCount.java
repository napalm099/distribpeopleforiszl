package data.db;

import data.struct.Divisions;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class DivisionCount {

    Connection connection;

    public DivisionCount() {
        this.connection = PgConn.getInstance().getConnection();

    }

    public Map<String, Integer> getCount(Divisions divisions) throws SQLException {
        Map<String, Integer> divisionCount = new HashMap<>();

        String query = String.format("select uch,count(*) from nase.all_ppl a " +
                "where date '%s' between date_in and date_out " +
                "and uch like '%%%s'" +
                "group by 1", LocalDate.now(), divisions.getDivCode());


        try (Statement st = connection.createStatement(); ResultSet rs = st.executeQuery(query)) {
            while (rs.next()) {

                String uch = rs.getString("uch");
                Integer count = rs.getInt("count");
                divisionCount.put(uch, count);
            }
        }

        return divisionCount;

    }

}
