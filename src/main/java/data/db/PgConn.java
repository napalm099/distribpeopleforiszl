package data.db;


import data.PropsReader;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class PgConn {

    private static final Logger logger = Logger.getLogger(PgConn.class);

    PropsReader propsReader = PropsReader.getInstance();
    final String DB_URL = propsReader.readParam("postgres.url");
    final String DB_USER = propsReader.readParam("postgres.user");
    final String DB_PASS = propsReader.readParam("postgres.pass");

    private Connection connection;

    private static PgConn pgConn = null;


    private PgConn() {
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
        } catch (ClassNotFoundException | SQLException e) {
            logger.error("no connection to DB",e);
            e.printStackTrace();
        }
    }


    public static PgConn getInstance() {
        if (pgConn == null)
            pgConn = new PgConn();
        return pgConn;
    }

    boolean status() {
        try {
            return connection.isValid(0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public Connection getConnection() {
        return connection;
    }


}
