package data.entities;

public class PlannedPplEntity extends PplEntity {


    private Integer month;


    public PlannedPplEntity(PplEntity pplEntity, Integer month) {
        super(pplEntity);
        this.month = month;

    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }
}
