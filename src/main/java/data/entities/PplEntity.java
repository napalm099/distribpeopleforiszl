package data.entities;

import java.time.LocalDate;

public class PplEntity {


    String enp;
    LocalDate dr;
    String uch;
    String smo;

    public PplEntity(String enp, LocalDate dr, String uch, String smo) {
        this.enp = enp;
        this.dr = dr;
        this.uch = uch;
        this.smo = smo;
    }

    PplEntity(PplEntity pplEntity) {
        this.enp = pplEntity.getEnp();
        this.dr = pplEntity.getDr();
        this.smo = pplEntity.getSmoName();
        this.uch = pplEntity.getUch();
    }

    public String getEnp() {
        return enp;
    }

    public LocalDate getDr() {
        return dr;
    }

    public String getUch() {
        return uch;
    }

    public String getSmoName() {
        return smo;
    }
}
