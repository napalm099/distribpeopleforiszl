package data.filesUtil;

import data.entities.PlannedPplEntity;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.log4j.Logger;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class CsvCreator {

    private static final Logger logger = Logger.getLogger(CsvCreator.class);
    List<PlannedPplEntity> plannedEntities;

    public CsvCreator(List<PlannedPplEntity> entities) {
        this.plannedEntities = entities;
    }

    CSVFormat getCsvFormat() {
        return CSVFormat.DEFAULT.withDelimiter(';');
    }

    public void append(String to) {
        logger.info(String.format("start writing result to file. %s rows",plannedEntities.size()));

        try (CSVPrinter printer = new CSVPrinter(new FileWriter(to), getCsvFormat())) {
            printer.printRecord("uch", "enp", "month");
            for (PlannedPplEntity e : plannedEntities
            ) {
                printer.printRecord(e.getUch(), e.getEnp(), e.getMonth());
            }
        } catch (IOException e) {
            logger.error("file could' not write",e);
            e.printStackTrace();
        }
    }

}
