package data.filesUtil;

import data.PropsReader;
import data.entities.PplEntity;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class CsvPplList {

    private static final Logger logger = Logger.getLogger(CsvPplList.class);

    CSVFormat cs;
    CSVParser parser;
    File csvFile;
    List<CSVRecord> csvRecordList;


    public CsvPplList() {

    }

    void getCSV() {


        String pathToFile = PropsReader.getInstance().readParam("file.allsmo");
        csvFile = new File(pathToFile);
    }

    void parseCSV() throws IOException {
        this.cs = CSVFormat.DEFAULT.withDelimiter(';').withFirstRecordAsHeader();
        parser = CSVParser.parse(csvFile, StandardCharsets.UTF_8, cs);
    }

    void readCSV() throws IOException {
        csvRecordList = parser.getRecords();
    }

    public List<PplEntity> getCSVList() {
        List<PplEntity> pplEntities = new ArrayList<>();
        getCSV();
        try {
            parseCSV();
            readCSV();
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (CSVRecord record : csvRecordList
        ) {
            pplEntities.add(new PplEntity(
                    record.get("ЕНП"),
                    LocalDate.parse(record.get("Дата_рождения"), DateTimeFormatter.ofPattern("yyyyMMdd")),
                    record.get("Участок"),
                    record.get("SMO")
            ));
        }
        return pplEntities;

    }


}
