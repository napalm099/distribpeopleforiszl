package data.struct;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import data.db.DivisionCount;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@JsonAutoDetect
public class Division {

    @JsonProperty("name")
    private Divisions named;
    //кол-во человек в плане
    private Map<Months, Integer> divisionMonthsPlan;
    //кол во чловек в базе по участкам
    private Map<String, Integer> divisionPplCount;

    @JsonProperty("sum")
    Integer planSum;

    @JsonProperty("name")
    public void unpackName(String s) {
        named = Divisions.getByVal(s);
    }

    @JsonProperty("divisionPlan")
    private void unpack(Map<String, Integer> valMap) {
        this.divisionMonthsPlan = new HashMap<>();
        valMap.forEach((key, value) -> divisionMonthsPlan.put(Months.getByVal(key), value));
    }

    public boolean isValid() {
        int result = divisionMonthsPlan.values().stream().mapToInt(Integer::intValue).sum();
        boolean b = result == planSum;
//        System.out.println(name + " : " + b +"   :  result "+ result+ " sum :"+ sum);
        return b;

    }

    @Override
    public String toString() {
        return "Division{" +
                "name='" + named + '\'' +
                ", divisionPlan=" + divisionMonthsPlan +
                ", sum=" + planSum +
                '}';
    }

    public Divisions getNamed() {
        return named;
    }

    public Map<Months, Integer> getDivisionMonthsPlan() {
        return divisionMonthsPlan;
    }

    public Integer getDivisionPlanByMonth(Months months) {
        return divisionMonthsPlan.get(months);
    }


    public Integer getPlanSum() {
        return planSum;
    }

    public Map<String, Integer> getDivisionDBPplCount() {
        if (divisionPplCount == null)
            try {
                divisionPplCount = new DivisionCount().getCount(this.named);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        return divisionPplCount;
    }


}
