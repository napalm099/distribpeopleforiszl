package data.struct;

import java.util.Arrays;

public enum Divisions {

    KORP1("korp1", "46"),
    KORP2("korp2", "47"),
    KORP3("korp3", "48"),
    KORP4("korp4", "136");

    private String litName;
    private String divCode;

    Divisions(String litName, String divCode) {
        this.litName = litName;
        this.divCode = divCode;
    }


    public String getLitName() {
        return litName;
    }

    public String getDivCode() {
        return divCode;
    }

    public void setName(String name, String divCode) {
        this.litName = name;
        this.divCode = divCode;
    }

    public static Divisions getByVal(String value) {
        return Arrays.stream(values()).filter(x -> x.getLitName().equals(value)).findFirst().orElse(null);
    }

}
