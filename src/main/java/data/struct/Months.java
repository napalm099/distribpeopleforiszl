package data.struct;

import java.util.Arrays;

public enum Months {

    JAN("jan"),
    FEB("feb"),
    MAR("mar"),
    APR("apr"),
    MAY("may"),
    JUN("jun"),
    JUL("jul"),
    AUG("aug"),
    SEP("sep"),
    OCT("oct"),
    NOV("nov"),
    DEC("dec");

    String val;

    Months(String val) {
        this.val = val;
    }


    public String getVal() {
        return val;
    }

    public static Months getByVal(String value) {
        return Arrays.stream(values()).filter(x -> x.getVal().equals(value)).findFirst().orElse(null);
    }
}
