package data.struct;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.stream.IntStream;

public class Plan {

    Integer yearOfPlan;
    PlanTypes type;
    Integer minAge;
    Integer maxAge;
    Integer intervalYear;

    @JsonProperty("sumAll")
    Integer sumAll;

    @JsonProperty("values")
    List<Division> divisionList;

    public PlanTypes getType() {
        return type;
    }
    @JsonProperty("type")
    public void setType(String type) {
        this.type = PlanTypes.getByVal(type);
    }

    public Integer getMinAge() {
        return minAge;
    }

    public void setMinAge(Integer minAge) {
        this.minAge = minAge;
    }

    public Integer getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(Integer maxAge) {
        this.maxAge = maxAge;
    }

    public Integer getIntervalYear() {
        return intervalYear;
    }

    public void setIntervalYear(Integer intervalYear) {
        this.intervalYear = intervalYear;
    }

    public Integer getSumAll() {
        return sumAll;
    }

    public void setSumAll(Integer sumAll) {
        this.sumAll = sumAll;
    }

    public List<Division> getDivisionList() {
        return divisionList;
    }

    public void setDivisionList(List<Division> divisionList) {
        this.divisionList = divisionList;
    }

    public Integer getYearOfPlan() {
        return yearOfPlan;
    }

    public void setYearOfPlan(Integer yearOfPlan) {
        this.yearOfPlan = yearOfPlan;
    }

    public boolean isValid() {

        boolean b = divisionList.stream().filter(Division::isValid).flatMapToInt(x -> IntStream.of(x.getPlanSum())).sum() == sumAll;
        boolean a = minAge < maxAge ^ minAge.equals(maxAge);
        return b & a;

    }


    @Override
    public String toString() {
        return "PlansDD{" +
                "type='" + type + '\'' +
                ", minAge=" + minAge +
                ", maxAge=" + maxAge +
                ", intervalYear=" + intervalYear +
                ", sumAll=" + sumAll +
                ", divisionList=" + divisionList +
                '}';
    }
}
