package data.struct;

import java.util.Arrays;

public enum PlanTypes {

    DD("dd", (i, min, iter) -> (i - min) % iter == 0 ),
    PMO("pmo", (i, min, iter) -> (i - min) % iter != 0 );

    String planName;
    public IterateCondition iterateCondition;

    PlanTypes(String planName, IterateCondition operator) {
        this.planName = planName;
        this.iterateCondition = operator;
    }

    public String getPlanName() {
        return planName;
    }

    public Boolean passedIterateCondition(Integer i, Integer minAge, Integer intervalYear) {
        return iterateCondition.passed(i, minAge, intervalYear);
    }

    public static PlanTypes getByVal(String value) {
        return Arrays.stream(values()).filter(x -> x.getPlanName().equals(value)).findFirst().orElse(null);
    }
}

interface IterateCondition {
    Boolean passed(Integer i, Integer min, Integer iter);
}
