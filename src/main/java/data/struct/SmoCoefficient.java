package data.struct;

public enum SmoCoefficient {


    INKO("inko", 0.6),

    SOGAZ("sogaz", 0.4);


    String litName;
    double weight;

    SmoCoefficient(String litName, double weight) {
        this.litName = litName;
        this.weight = weight;
    }

    public String getLitName() {
        return litName;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }
}
