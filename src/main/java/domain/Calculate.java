package domain;

import data.entities.PlannedPplEntity;
import data.entities.PplEntity;
import data.filesUtil.CsvCreator;
import data.filesUtil.CsvPplList;
import data.struct.Division;
import data.struct.Months;
import data.struct.Plan;
import data.struct.SmoCoefficient;
import org.apache.log4j.Logger;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Calculate {

    private static final Logger logger = Logger.getLogger(Calculate.class);

    Plan plan;
    CsvPplList csvPplList;
    String saveTo;
    List<PplEntity> pplEntityQueue;

    Set<PplEntity> checkedPeople = new HashSet<>();

    public Calculate(Plan plan, CsvPplList csvPplList, String saveTo) {
        this.plan = plan;
        this.csvPplList = csvPplList;
        this.saveTo = saveTo;
    }


    public void calc() {

        Queue<PplEntity> pplEntityQueue = new LinkedList<>(csvPplList.getCSVList());
        this.pplEntityQueue = (List) pplEntityQueue;
        logger.info("people available: " + pplEntityQueue.size());
        List<PlannedPplEntity> plannedEntities = new ArrayList<>();
        for (Division division : plan.getDivisionList()
        ) {
            for (SmoCoefficient smo : SmoCoefficient.values()
            ) {
                for (Months month : Months.values()
                ) {
                    logger.info(String.format("assembling: %s %s %s", smo, month, division.getNamed()));
                    int divMonthPlan = division.getDivisionPlanByMonth(month);
                    if (divMonthPlan == 0) {
                        logger.info(String.format("value of %s plan is null - skip", month));
                        continue;
                    }


                    Map<String, Long> countUchMonthPlan = getUchsCoefficient(division).entrySet().stream()
                            .collect(Collectors.toMap(Map.Entry::getKey, x -> Math.round(x.getValue() * divMonthPlan)));
//                            .collect(Collectors.toMap(Map.Entry::getKey, x -> Math.round(x.getValue())));
                    System.out.println(countUchMonthPlan);
                    int counter = plannedEntities.size();

                    for (Map.Entry<String, Long> planEntry : countUchMonthPlan.entrySet()) {
//                        System.out.println(planEntry);
                        List<PlannedPplEntity> res = getPlannedEntities(smo, division, planEntry, month);
//                        logger.info(String.format("uch: %s; plan: %d; people added: %s; %b ",planEntry.getKey(), planEntry.getValue(), res.size(),planEntry.getValue()==res.size() ));
                        plannedEntities.addAll(res);
                    }
                    logger.info(String.format("rows added: %s", plannedEntities.size() - counter));

                }
            }

        }
        new CsvCreator(plannedEntities).append(saveTo);
    }

    private List<PlannedPplEntity> getPlannedEntities(SmoCoefficient smo, Division division, Map.Entry<String, Long> planEntry, Months month) {

        List<PplEntity> entities = getListOfPlannedSubjectsByDivision(division);

        long limit = Math.round(planEntry.getValue() * smo.getWeight());
        int i = 0;
        List<PlannedPplEntity> plannedPplEntities = new ArrayList<>();
        for (PplEntity entity : entities
        ) {
            if (!checkedPeople.contains(entity)
                    & entity.getUch().equals(planEntry.getKey())
                    & smo.getLitName().equals(entity.getSmoName())) {

                if (i < limit) {
                    plannedPplEntities.add(new PlannedPplEntity(entity, month.ordinal()));
                    checkedPeople.add(entity);
                    i++;
                } else break;
            }
        }

        return plannedPplEntities;


//        return getListOfPlannedSubjectsByDivision(division).stream()
//                .filter(x -> x.getUch().equals(planEntry.getKey())
//                        & smo.getLitName().equals(x.getSmoName()))
//                .limit(Math.round(planEntry.getValue() * smo.getWeight()))
//                .collect(ArrayList::new
//                        , (list, item) -> list.add(new PlannedPplEntity(item, month.ordinal()))
//                        , ArrayList::addAll);
    }


    //возвращает ряд подходящих по критерию плана возрастов
    private List<Integer> getAgesRange() {

        return IntStream
                .rangeClosed(plan.getMinAge(), plan.getMaxAge())
                .filter(i -> plan.getType().passedIterateCondition(i, plan.getMinAge(), plan.getIntervalYear()))
                .boxed()
                .collect(Collectors.toList());
    }


    private List<PplEntity> getListOfPlannedSubjects() {
        List<PplEntity> pplEntities = csvPplList.getCSVList();

        return pplEntities.stream()
                .filter(x -> getAgesRange().contains(plan.getYearOfPlan() - x.getDr().getYear()))
                .filter(x -> !x.getUch().endsWith("49"))
                .collect(Collectors.toList());
    }

    //возвращает очередь человек подходящих по условиям плана(возраст, участок)
    private List<PplEntity> getListOfPlannedSubjectsByDivision(Division division) {
        return pplEntityQueue.stream()
                .filter(x -> getAgesRange().contains(plan.getYearOfPlan() - x.getDr().getYear()))
                .filter(x -> !x.getUch().endsWith("49"))
                .filter(x -> x.getUch().endsWith(division.getNamed().getDivCode()))
                .collect(LinkedList::new, LinkedList::add, LinkedList::addAll);
    }

    //возвращает пары учсток - коэффициент (количество человек на участке относительно общего количества)
    private Map<String, Double> getUchsCoefficient(Division division) {

        int sum = division.getDivisionDBPplCount().values().stream().mapToInt(Integer::intValue).sum();
//        logger.info(String.format("%s %s",division.getNamed(),sum));
        return division.getDivisionDBPplCount().entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, x -> Double.valueOf(x.getValue()) / sum));
    }


}
